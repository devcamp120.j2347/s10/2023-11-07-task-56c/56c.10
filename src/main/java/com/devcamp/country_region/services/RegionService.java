package com.devcamp.country_region.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.country_region.models.Region;

@Service
public class RegionService {
    private Region Hanoi = new Region("HN", "Ha Noi");
    private Region Hochiminh = new Region("HCM", "Ho Chi Minh");

    private Region Tokyo = new Region("TKO", "Tokyo");
    private Region Osaka = new Region("OSK", "Osaka");

    public ArrayList<Region> getVietnamRegions() {
        ArrayList<Region> regions = new ArrayList<>();

        regions.add(Hanoi);
        regions.add(Hochiminh);

        return regions;
    }

    public ArrayList<Region> getJapanRegions() {
        ArrayList<Region> regions = new ArrayList<>();

        regions.add(Tokyo);
        regions.add(Osaka);

        return regions;
    }

    public Region getRegionByRegionCode(String regionCode) {
        for (Region region : getJapanRegions()) {
            if (region.getRegionCode().equals(regionCode)) {
                return region;
            }
        }

        for (Region region : getVietnamRegions()) {
            if (region.getRegionCode().equals(regionCode)) {
                return region;
            }
        }

        return null;
    }
}
