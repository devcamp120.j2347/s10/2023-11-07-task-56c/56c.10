package com.devcamp.country_region.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.country_region.models.Country;

@Service
public class CountryService {
    @Autowired
    private RegionService regionService;

    private Country Vietnam = new Country("VN", "Viet Nam");
    private Country Japan = new Country("JP", "Japan");

    public ArrayList<Country> getCountries() {
        ArrayList<Country> countries = new ArrayList<>();

        Vietnam.setRegions(this.regionService.getVietnamRegions());
        Japan.setRegions(this.regionService.getJapanRegions());

        countries.add(Vietnam);
        countries.add(Japan);

        return countries;
    }

    public Country getCountryByCountryCode(String countryCode) {
        ArrayList<Country> countries = this.getCountries();

        for (Country country : countries) {
            if (country.getCountryCode().equals(countryCode)) {
                return country;
            }
        }

        return null;
    }
}
