package com.devcamp.country_region.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.country_region.models.Region;
import com.devcamp.country_region.services.RegionService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class RegionController {
    
    @Autowired
    private RegionService regionService;

    @RequestMapping("/region-info")
    public Region getRegionByRegionCode(@RequestParam String regionCode) {
        Region result = this.regionService.getRegionByRegionCode(regionCode);

        return result;

    }
}
